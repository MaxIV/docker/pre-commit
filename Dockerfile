FROM ghcr.io/mamba-org/micromamba:latest

LABEL MAINTAINER "kits-sw@maxiv.lu.se"

ENV PRE_COMMIT_VERSION 4.1.0

USER root
RUN apt update && \
  apt install -y git && \
  apt clean && \
  rm -rf /var/lib/apt/lists/*

# Avoid: fatal: detected dubious ownership in repository
RUN git config --system --add safe.directory '*'

USER mambauser
# Install several Python versions
RUN micromamba create -y -n python39 -c conda-forge python=3.9 \
  && micromamba create -y -n python310 -c conda-forge python=3.10 \
  && micromamba create -y -n python311 -c conda-forge python=3.11 \
  && micromamba create -y -n python312 -c conda-forge python=3.12 pre-commit=${PRE_COMMIT_VERSION} \
  && micromamba create -y -n python313 -c conda-forge python=3.13 \
  && micromamba clean -ay

# Use 3.9 as default python for now
ENV PATH /opt/conda/envs/python39/bin:/opt/conda/envs/python310/bin:/opt/conda/envs/python311/bin:/opt/conda/envs/python312/bin:/opt/conda/envs/python313/bin:$PATH
