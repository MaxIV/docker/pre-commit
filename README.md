# pre-commit docker image

Python docker image with [pre-commit](https://pre-commit.com) included,
as well as several versions of Python.

This image is used to run all pre-commit hooks in GitLab CI.
Your project should include a `.pre-commit-config.yaml` file.

Docker pull command:

```bash
docker pull registry.gitlab.com/maxiv/docker/pre-commit
```
